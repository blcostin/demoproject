package com.example.adonisandroid.aroundwisedemoproject.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import com.example.adonisandroid.aroundwisedemoproject.DemoApp;
import com.example.adonisandroid.aroundwisedemoproject.UserPrefs;
import com.example.adonisandroid.aroundwisedemoproject.room.UserDatabase;

import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity {

    protected static final String PHONE_NUMBER_TAG = "4356";
    protected static final String PASSWORD_TAG = "5681";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(BaseActivity.this);
    }

    @LayoutRes protected abstract int getLayoutId();

    protected UserDatabase getDatabase() {
        return ((DemoApp) getApplication()).getUserDatabase();
    }

    protected UserPrefs getUserPrefs() {
        return getDemoApp().getUserPrefs();
    }

    public DemoApp getDemoApp() {
        return ((DemoApp) getApplication());
    }

}
