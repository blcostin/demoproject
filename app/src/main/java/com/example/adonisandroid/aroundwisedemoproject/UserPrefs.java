package com.example.adonisandroid.aroundwisedemoproject;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.adonisandroid.aroundwisedemoproject.room.UserEntry;
import com.google.gson.Gson;

public class UserPrefs {

    private static final String PREFS_KEY = "key.user.prefs";
    private static final String LOGGED_IN_KEY = "key.logged.in";
    private static final String USER_KEY = "key.user";
    private SharedPreferences prefs;

    UserPrefs(Context context) {
        prefs = context.getSharedPreferences(PREFS_KEY, Context.MODE_PRIVATE);
    }

    public void logout() {
        setUserLoggedIn(false);
        prefs.edit().remove(USER_KEY).apply();
    }

    public boolean isUserLoggedIn() {
        return prefs.getBoolean(LOGGED_IN_KEY, false);
    }

    public void setUserLoggedIn(final boolean isLoggedIn) {
        prefs.edit().putBoolean(LOGGED_IN_KEY, isLoggedIn).apply();
    }

    public void setCurrentUser(final UserEntry user) {
        prefs.edit().putString(USER_KEY, userEntryToJson(user)).apply();
    }

    public UserEntry getCurrentUser() {
        return getUserEntryFromJsonStr(prefs.getString(USER_KEY, ""));
    }

    private UserEntry getUserEntryFromJsonStr(final String jsonStr) {
        Gson gson = new Gson();
        return gson.fromJson(jsonStr, UserEntry.class);
    }

    private String userEntryToJson(final UserEntry userEntry) {
        Gson gson = new Gson();
        return gson.toJson(userEntry, UserEntry.class);
    }
}
