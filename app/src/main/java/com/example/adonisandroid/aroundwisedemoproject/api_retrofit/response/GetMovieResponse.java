package com.example.adonisandroid.aroundwisedemoproject.api_retrofit.response;

import com.example.adonisandroid.aroundwisedemoproject.api_retrofit.model.Movie;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetMovieResponse {

    @SerializedName("page")
    @Expose
    public Integer page;

    @SerializedName("results")
    @Expose
    private List<Movie> results = null;

    public Integer getPage() {
        return page;
    }

    public List<Movie> getResults() {
        return results;
    }
}
