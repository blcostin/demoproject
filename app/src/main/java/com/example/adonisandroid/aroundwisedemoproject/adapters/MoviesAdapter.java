package com.example.adonisandroid.aroundwisedemoproject.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.adonisandroid.aroundwisedemoproject.R;
import com.example.adonisandroid.aroundwisedemoproject.api_retrofit.model.Movie;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public abstract class MoviesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<Movie> moviesList;
    protected Context context;
    RecyclerViewListener listener;

    MoviesAdapter(List<Movie> moviesList, Context context, RecyclerViewListener listener){
        this.moviesList = moviesList;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public abstract RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType);

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {

        BindMovieInterface movieInterface = (BindMovieInterface) holder;
        movieInterface.bindData(moviesList.get(i));
    }


    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    @Override
    public abstract int getItemViewType(int position);

    class MovieViewHolderSmall extends RecyclerView.ViewHolder implements BindMovieInterface{


        @BindView(R.id.item_title)
        TextView movieTitle;

        @BindView(R.id.item_poster)
        ImageView moviePoster;

        @BindView(R.id.item_likes)
        TextView movieLikes;


        MovieViewHolderSmall(View view, final RecyclerViewListener listener) {
            super(view);

            ButterKnife.bind(this,view);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(view, MovieViewHolderSmall.this.getAdapterPosition());
                }
            });

        }

        @SuppressLint("SetTextI18n")
        @Override
        public void bindData(Movie movie) {
            movieTitle.setText(movie.getTitle());
            movieLikes.setText(movie.getVoteCount().toString());
            movieTitle.setSelected(true);

            Glide.with(context)
                    .load("http://image.tmdb.org/t/p/w500/"+movie.getPosterPath())
                    .into(moviePoster);
        }
    }

    protected interface BindMovieInterface{
        void bindData(Movie movie);
    }
}