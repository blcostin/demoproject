package com.example.adonisandroid.aroundwisedemoproject.login.repository.userentry;

import android.annotation.SuppressLint;
import android.os.AsyncTask;

import com.example.adonisandroid.aroundwisedemoproject.login.repository.DataListener;
import com.example.adonisandroid.aroundwisedemoproject.room.UserDatabase;
import com.example.adonisandroid.aroundwisedemoproject.room.UserEntry;

import java.lang.ref.WeakReference;

public class UserEntryRepository implements UserEntryDataSource {

    private final UserDatabase userDatabase;

    public UserEntryRepository(UserDatabase userDatabase) {
        this.userDatabase = userDatabase;
    }

    @Override
    public void getUserEntry(final String phoneNumber, final DataListener<UserEntry> dataListener) {
        final WeakReference<DataListener<UserEntry>> dataListenerWeakReference = new WeakReference<>(dataListener);
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, UserEntry> getUserEntryTask = new AsyncTask<Void, Void, UserEntry>() {
            @Override
            protected UserEntry doInBackground(Void... voids) {
                return userDatabase.userDao().findUser(phoneNumber);
            }

            @Override
            protected void onPostExecute(UserEntry userEntry) {
                super.onPostExecute(userEntry);
                if (dataListenerWeakReference.get() != null) {
                    dataListenerWeakReference.get().onSuccess(userEntry);
                }
            }
        };
        getUserEntryTask.execute();
    }


    @Override
    public void insertUser(final UserEntry userEntry) {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, UserEntry> insertUserEntry = new AsyncTask<Void, Void, UserEntry>() {
            @Override
            protected UserEntry doInBackground(Void... voids) {
                userDatabase.userDao().insertUser(userEntry);
                return null;
            }

            @Override
            protected void onPostExecute(UserEntry userEntry) {
                super.onPostExecute(userEntry);
            }
        };
        insertUserEntry.execute();
    }

    @Override
    public void updateUser(final UserEntry userEntry, final DataListener<UserEntry> dataListener) {
        @SuppressLint("StaticFieldLeak") AsyncTask<Void, Void, UserEntry> updateUserEntry = new AsyncTask<Void, Void, UserEntry>() {
            final WeakReference<DataListener<UserEntry>> dataListenerWeakReference = new WeakReference<>(dataListener);
            @Override
            protected UserEntry doInBackground(Void... voids) {
                userDatabase.userDao().updateUser(userEntry);
                if (dataListenerWeakReference.get() != null) {
                    dataListenerWeakReference.get().onSuccess(userEntry);
                }
                return null;
            }

            @Override
            protected void onPostExecute(UserEntry userEntry) {
                super.onPostExecute(userEntry);
            }
        };
        updateUserEntry.execute();
    }
}
