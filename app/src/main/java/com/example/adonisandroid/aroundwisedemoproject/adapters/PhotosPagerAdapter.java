package com.example.adonisandroid.aroundwisedemoproject.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.example.adonisandroid.aroundwisedemoproject.R;

public class PhotosPagerAdapter extends PagerAdapter{

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private String url;

    public PhotosPagerAdapter(Context context,String url) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.url = url;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view ==  object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.pager_item_photo, container, false);

        ImageView imageView = itemView.findViewById(R.id.imageView);

        Glide.with(mContext)
                .load(url)
                .into(imageView);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}