package com.example.adonisandroid.aroundwisedemoproject.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.adonisandroid.aroundwisedemoproject.R;
import com.example.adonisandroid.aroundwisedemoproject.api_retrofit.model.Movie;

import java.util.List;

public class MovieProfileAdapter extends MoviesAdapter{
    public MovieProfileAdapter(List<Movie> moviesList, Context context, RecyclerViewListener listener) {
        super(moviesList, context, listener);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View itemView;

        itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.small_item_layout, viewGroup, false);
        return new MovieViewHolderSmall(itemView, listener);
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.small_item_layout;
    }
}
