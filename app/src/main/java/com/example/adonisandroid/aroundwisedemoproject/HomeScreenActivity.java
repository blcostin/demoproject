package com.example.adonisandroid.aroundwisedemoproject;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.adonisandroid.aroundwisedemoproject.api_retrofit.model.Movie;
import com.example.adonisandroid.aroundwisedemoproject.api_retrofit.response.GetMovieResponse;
import com.example.adonisandroid.aroundwisedemoproject.base.BaseActivity;
import com.example.adonisandroid.aroundwisedemoproject.homescreenfragments.DiscoverFragment;
import com.example.adonisandroid.aroundwisedemoproject.homescreenfragments.MapsFragment;
import com.example.adonisandroid.aroundwisedemoproject.homescreenfragments.OfferDetailsFragment;
import com.example.adonisandroid.aroundwisedemoproject.homescreenfragments.ProfileFragment;
import com.example.adonisandroid.aroundwisedemoproject.homescreenfragments.QRScreen;
import com.example.adonisandroid.aroundwisedemoproject.homescreenfragments.SettingsFragment;
import com.example.adonisandroid.aroundwisedemoproject.room.UserEntry;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.Serializable;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeScreenActivity extends BaseActivity {

    @BindView(R.id.navigation)
    BottomNavigationView bottomNavigationView;

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @BindView(R.id.action_item1)
    View previousItem;

    @BindView(R.id.action_item4)
    ImageView itemProfile;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.edit_search_layout)
    LinearLayout search_layout;


    private GetMovieResponse getMoviesResponse;
    private IntentIntegrator qrScanner;
    private TextView actionBarTitle;
    private View backButtonActionBar;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);

        //center the title at action bar
        Objects.requireNonNull(getSupportActionBar()).setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.action_bar_label_layout);
        actionBarTitle = getSupportActionBar().getCustomView().findViewById(R.id.action_bar_label);
        backButtonActionBar = getSupportActionBar().getCustomView().findViewById(R.id.back_button_layout);

        backButtonActionBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager().popBackStack();
            }
        });

        //initialize tab layout
        setTabLayoutTitles();

        //initializing baground for the first item in the bottom bar
        previousItem.setBackgroundResource(R.drawable.bottom_bar);

        itemProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (getUserPrefs().isUserLoggedIn()) {
                    if(previousItem!=null) {
                        previousItem.setBackgroundResource(0);
                        previousItem = null;
                    }
                    changeFragment(ProfileFragment.newInstance((Serializable) getMoviesResponse.getResults()));
                    setTabLayoutInvisible();
                } else
                    makeToast(getString(R.string.you_are_not_logged_in)).show();
            }
        });

        loadProfilePicture(itemProfile);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                List<Movie> movieList = sortItemsByFilter(tab.getPosition());
                changeFragment(DiscoverFragment.newInstance((Serializable)movieList));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });


        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        View view;

                        view = findViewById(item.getItemId());

                        if(previousItem!=null)
                            previousItem.setBackgroundResource(0);

                        view.setBackgroundResource(R.drawable.bottom_bar);

                            switch (item.getItemId()) {
                                case R.id.action_item1:
                                    selectedFragment = DiscoverFragment.newInstance((Serializable) getMoviesResponse.getResults());
                                    setTabLayoutVisible();
                                    break;
                                case R.id.action_item2:
                                    changeActionBarItems(false,false,getString(R.string.loyality),false);
                                    qrScanner = new IntentIntegrator(HomeScreenActivity.this);
                                    qrScanner.setPrompt(getString(R.string.place_qr_code_inside));
                                    qrScanner.setOrientationLocked(true);
                                    qrScanner.setCaptureActivity(QRScreen.class);
                                    qrScanner.initiateScan();
                                    break;
                                case R.id.action_item3:
                                    selectedFragment = MapsFragment.newInstance();
                                    break;
                            }

                            if(item.getItemId()!=R.id.action_item2)
                                changeFragment(selectedFragment);

                                previousItem = view;

                            if(view.getId()==R.id.action_item3)
                                setTabLayoutInvisible();

                        return true;
                    }
                });


        Call<GetMovieResponse> responseCall = ((DemoApp) getApplication()).getWebServices().getMovies(getString(R.string.api_key));

        responseCall.enqueue(new Callback<GetMovieResponse>() {
            @Override
            public void onResponse(@NonNull Call<GetMovieResponse> call, @NonNull Response<GetMovieResponse> response) {
                getMoviesResponse = response.body();

                hideProgressBar();

                //initialize the view with the discover fragment
                List<Movie> movieList = sortItemsByFilter(0);
                changeFragment(DiscoverFragment.newInstance((Serializable) movieList));
            }

            @Override
            public void onFailure(@NonNull Call<GetMovieResponse> call, @NonNull Throwable t) {
                makeToast("Network error occured").show();
            }
        });
    }

    private void setTabLayoutTitles() {
        tabLayout.addTab(tabLayout.newTab().setText(R.string.all));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.coupons));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.last_minute));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.news));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.sales));
    }

    public void setTabLayoutInvisible(){
        tabLayout.setVisibility(View.GONE);
        search_layout.setVisibility(View.GONE);
    }

    public void setTabLayoutVisible(){
        tabLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==R.id.settings_menu_icon)
            changeFragment(SettingsFragment.newInstance());
        else if(item.getItemId()==R.id.search_menu_icon)
            search_layout.setVisibility(View.VISIBLE);
        else actionBarTitle.setVisibility(View.VISIBLE);

        return true;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_home_screen;
    }

    public void changeFragment(Fragment selectedFragment){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        assert selectedFragment != null;
        transaction.replace(R.id.frame_layout, selectedFragment);

        if(selectedFragment instanceof SettingsFragment || selectedFragment instanceof OfferDetailsFragment)
            transaction.addToBackStack(null);
        transaction.commit();
    }

    public Toast makeToast(String message){
        return Toast.makeText(HomeScreenActivity.this, message, Toast.LENGTH_SHORT);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null && result.getContents()!=null) {
            if(Patterns.WEB_URL.matcher(result.getContents()).matches()) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(result.getContents()));
                startActivity(browserIntent);
            }
            else makeToast(getString(R.string.not_valid_url)).show();
        }
        else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void loadProfilePicture(ImageView itemProfile) {

        UserEntry currentUser = getUserPrefs().getCurrentUser();
        if(currentUser!=null) {
            if (currentUser.getPictureUrl() != null)
                Glide.with(this)
                        .load(currentUser.getPictureUrl())
                        .into(itemProfile);
        }
    }

    public void changeActionBarItems(boolean search,boolean settings, String title, boolean visibility){
        menu.getItem(0).setVisible(search);
        menu.getItem(1).setVisible(settings);
        actionBarTitle.setText(title);
        if (visibility)
            backButtonActionBar.setVisibility(View.VISIBLE);
        else
            backButtonActionBar.setVisibility(View.INVISIBLE);
    }

    private void hideProgressBar(){
        progressBar.setVisibility(View.GONE);
    }


    private List<Movie> sortItemsByFilter(int i) {

        List<Movie> movieList = getMoviesResponse.getResults();

        switch (i) {
            case 0:
                Collections.sort(movieList, new Comparator<Movie>() {
                    @Override
                    public int compare(Movie movie, Movie movie2) {
                        return (movie.getTitle().compareTo(movie2.getTitle()));
                }
            });
                break;

            case 1:
                Collections.sort(movieList, new Comparator<Movie>() {
                    @Override
                    public int compare(Movie movie, Movie movie2) {
                        return (movie.getVoteCount() - movie2.getVoteCount());
                    }
                });
                break;

            case 2:
                Collections.sort(movieList, new Comparator<Movie>() {
                    @Override
                    public int compare(Movie movie, Movie movie2) {
                        return (movie.getId() - movie2.getId());
                    }
                });
                break;

            case 3:
                Collections.sort(movieList, new Comparator<Movie>() {
                    @Override
                    public int compare(Movie movie, Movie movie2) {
                        return (int) (movie.getPopularity() - movie2.getPopularity());
                    }
                });
                break;

            case 4:
                Collections.sort(movieList, new Comparator<Movie>() {
                    @Override
                    public int compare(Movie movie, Movie movie2) {
                        return (int) (movie.getVoteAverage() - movie2.getVoteAverage());
                    }
                });
                break;

            default: break;
        }

        return movieList;
    }
}