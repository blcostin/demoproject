package com.example.adonisandroid.aroundwisedemoproject.login;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.example.adonisandroid.aroundwisedemoproject.HomeScreenActivity;
import com.example.adonisandroid.aroundwisedemoproject.base.BaseActivity;
import com.example.adonisandroid.aroundwisedemoproject.room.UserEntry;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;

import com.example.adonisandroid.aroundwisedemoproject.R;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FillProfileInfoActivity extends BaseActivity {


    @BindView(R.id.first_name_data)
    protected EditText firstName;

    @BindView(R.id.last_name_data)
    EditText lastName;

    @BindView(R.id.email_data)
    EditText emailAdress;

    private CallbackManager callbackManager;
    private UserEntry userEntry;
    private String phoneNumber;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);

        userEntry = new UserEntry();

        phoneNumber = getIntent().getStringExtra(SignInUpActivity.PHONE_NUMBER_TAG);
        password = getIntent().getStringExtra(SetPasswordActivity.PASSWORD_TAG);

        callbackManager = CallbackManager.Factory.create();
        registerCallback();
    }


    private void registerCallback() {
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        getFacebookUserCredentials(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        exception.printStackTrace();
                    }
                });
    }


    @Override
    //passes the facebook login result to the LoginManager via callbackManager
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void getFacebookUserCredentials(final AccessToken facebookAccessToken) {
        GraphRequest graphRequest = GraphRequest.newMeRequest(facebookAccessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    if (object == null) {
                        return;
                    }

                    JSONObject pictureJson = object.getJSONObject("picture").getJSONObject("data");
                    String pictureUrl = pictureJson.getString("url");

                    userEntry.setEmailAdress((object.has("email")) ? object.getString("email") : "");
                    userEntry.setFirstName(object.getString("first_name"));
                    userEntry.setLastName(object.getString("last_name"));
                    userEntry.setGender(object.has("gender") ? object.getString("gender"): "");
                    userEntry.setBirthDate(object.has("birthday") ? object.getString("birthday"): "");
                    userEntry.setPhoneNumber(phoneNumber);
                    userEntry.setPassword(password);
                    userEntry.setPictureUrl(pictureUrl);

                    getDemoApp().getUserEntryDataSource().insertUser(userEntry);

                    getUserPrefs().setCurrentUser(userEntry);
                    getUserPrefs().setUserLoggedIn(true);
                    goToDiscoverActivity();
                } catch (JSONException e) {
                    facebookError().show();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,first_name,last_name,picture,birthday,gender");
        graphRequest.setParameters(parameters);
        graphRequest.executeAsync();
    }

    private Toast showWrongEmailFormat() { return Toast.makeText(this, R.string.wrong_email_format,Toast.LENGTH_SHORT);
    }

    private Toast showFirstNameIsEmpty() {  return Toast.makeText(this, R.string.empty_field_warning1,Toast.LENGTH_SHORT);
    }

    private Toast showLastNameIsEmpty() {  return Toast.makeText(this, R.string.empty_field_warning2,Toast.LENGTH_SHORT);
    }

    private Toast facebookError() {  return Toast.makeText(this, R.string.facebook_error,Toast.LENGTH_SHORT);
    }

    private boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        return email.matches(expression);
    }


    @OnClick(R.id.register_button)
    protected void validate() {

        if(firstName.getText().toString().isEmpty())
            showFirstNameIsEmpty().show();
            else if(lastName.getText().toString().isEmpty())
                showLastNameIsEmpty().show();
                else if(!isEmailValid(emailAdress.getText().toString()))
                    showWrongEmailFormat().show();
                    else {
                        setUserProfile();
                        getDemoApp().getUserEntryDataSource().insertUser(userEntry);

                        getUserPrefs().setCurrentUser(userEntry);
                        getUserPrefs().setUserLoggedIn(true);
                        goToDiscoverActivity();
                    }
    }

    @OnClick(R.id.facebook_button)
    public void importFromFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile","email","user_birthday","user_gender"));
    }

    private void goToDiscoverActivity() {
        Intent intent = new Intent(this, HomeScreenActivity.class);
        startActivity(intent);
        finish();
    }

    private void setUserProfile() {
        userEntry.setFirstName(firstName.getText().toString());
        userEntry.setLastName(lastName.getText().toString());
        userEntry.setEmailAdress(emailAdress.getText().toString());
        userEntry.setPhoneNumber(phoneNumber);
        userEntry.setPassword(password);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_fill_profile_info;
    }
}
