package com.example.adonisandroid.aroundwisedemoproject.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.adonisandroid.aroundwisedemoproject.R;
import com.example.adonisandroid.aroundwisedemoproject.api_retrofit.model.Movie;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieDiscoverAdapter extends MoviesAdapter {
    public MovieDiscoverAdapter(List<Movie> moviesList, Context context, RecyclerViewListener listener) {
        super(moviesList, context, listener);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        View itemView;

        if(viewType== R.layout.small_item_layout) {
            itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.small_item_layout, viewGroup, false);
            return new MovieViewHolderSmall(itemView, listener);
        }

        else{
            itemView = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.big_item_layout, viewGroup, false);
            return new MovieViewHolderBig(itemView, listener);
        }

    }

    @Override
    public int getItemViewType(int position) {
        if(position % 3==0)
            return R.layout.big_item_layout;
        return R.layout.small_item_layout;
    }



    class MovieViewHolderBig extends RecyclerView.ViewHolder implements BindMovieInterface{


        @BindView(R.id.item_title)
        TextView movieTitle;

        @BindView(R.id.item_poster)
        ImageView moviePoster;

        @BindView(R.id.item_details)
        TextView movieOverview;

        @BindView(R.id.item_likes)
        TextView movieLikes;


        MovieViewHolderBig(View view, final RecyclerViewListener listener) {
            super(view);

            ButterKnife.bind(this,view);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(view, MovieViewHolderBig.this.getAdapterPosition());
                }
            });
        }


        @SuppressLint("SetTextI18n")
        @Override
        public void bindData(Movie movie) {
            movieTitle.setText(movie.getTitle());
            movieLikes.setText(movie.getVoteCount().toString());
            movieTitle.setSelected(true);
            movieOverview.setText(movie.getOverview());

            Glide.with(context)
                    .load("http://image.tmdb.org/t/p/w500/"+movie.getPosterPath())
                    .into(moviePoster);
        }
    }

}
