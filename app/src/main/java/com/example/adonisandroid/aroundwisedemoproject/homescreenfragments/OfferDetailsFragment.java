package com.example.adonisandroid.aroundwisedemoproject.homescreenfragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.adonisandroid.aroundwisedemoproject.HomeScreenActivity;
import com.example.adonisandroid.aroundwisedemoproject.R;
import com.example.adonisandroid.aroundwisedemoproject.adapters.PhotosPagerAdapter;
import com.example.adonisandroid.aroundwisedemoproject.api_retrofit.model.Movie;

import java.io.Serializable;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class OfferDetailsFragment extends Fragment {

    @BindView(R.id.item_title)
    TextView title;

    @BindView(R.id.item_likes)
    TextView likes;

    @BindView(R.id.item_details)
    TextView overview;

    @BindView(R.id.photo_pager)
    ViewPager poster;

    @BindView(R.id.offerVideo)
    VideoView video;

    @OnClick(R.id.item_maps_icon)
        protected void sentToLink(){
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://en.wikipedia.org/wiki/"+movie.getTitle()));
            startActivity(browserIntent);
        }

    @OnClick(R.id.item_video_icon)
        protected void viewVideo(){

            poster.setVisibility(View.GONE);
            video.setVisibility(View.VISIBLE);
            video.setVideoURI(Uri.parse("android.resource://" + context.getPackageName() +"/"+R.raw.countdown));
            video.setMediaController(new MediaController(context));
            video.requestFocus();
            video.start();
    }


    private static final String ARG_TAG = "6011";
    private Context context;
    private Movie movie;

    public OfferDetailsFragment() {
        // Required empty public constructor
    }

    public static OfferDetailsFragment newInstance(Serializable movie) {
        OfferDetailsFragment offerDetailsFragment =  new OfferDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_TAG, movie);
        offerDetailsFragment.setArguments(args);
        return offerDetailsFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments()!=null)
            movie = (Movie) getArguments().getSerializable(ARG_TAG);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_offer_details, container, false);
        ButterKnife.bind(this,v);
        hideKeyboard(v);
        return v;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((HomeScreenActivity) Objects.requireNonNull(getContext())).changeActionBarItems(false, false, getString(R.string.offers), true);

        title.setText(movie.getTitle());
        overview.setText(movie.getOverview());
        likes.setText(movie.getVoteCount().toString());

        PagerAdapter pagerAdapter = new PhotosPagerAdapter(context, "https://image.tmdb.org/t/p/w500/"+movie.getPosterPath());
        poster.setAdapter(pagerAdapter);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void hideKeyboard(View view) {
        InputMethodManager keyboard = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        assert keyboard != null;
        keyboard.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
