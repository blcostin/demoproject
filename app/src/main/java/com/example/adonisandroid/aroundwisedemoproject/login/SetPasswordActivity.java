package com.example.adonisandroid.aroundwisedemoproject.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.adonisandroid.aroundwisedemoproject.R;
import com.example.adonisandroid.aroundwisedemoproject.base.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SetPasswordActivity extends BaseActivity {

    @BindView(R.id.password_confirmation_text)
    EditText passwordValidation;

    @BindView(R.id.password_confirmation_text2)
    EditText passwordValidation2;

    @BindView(R.id.next_button)
    Button nextButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);

        //get phone number from SignIn screen
        Intent intent = getIntent();
        final String phoneNumber = intent.getStringExtra(SignInUpActivity.PHONE_NUMBER_TAG);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String password1  = passwordValidation.getText().toString();
                String password2 = passwordValidation2.getText().toString();

                if(validatePassword(password1)){
                    if(checkPasswords(password1,password2)) {
                        goToFillProfileInfoScreen(phoneNumber, password1);
                        finish();
                    }
                    else makeWarningToast(getResources().getString(R.string.invalid_password_data)).show();
                }
                else makeWarningToast(getString(R.string.minimum_characters)).show();
            }
        });
    }


    private boolean checkPasswords(String password1, String password2){
        return password1.equals(password2);
    }

    private boolean validatePassword(String password){
        return password.length()>=8;
    }

    private void goToFillProfileInfoScreen(String phoneNumber, String password){
        Intent intent = new Intent(this,FillProfileInfoActivity.class);
        intent.putExtra(SignInUpActivity.PHONE_NUMBER_TAG,phoneNumber);
        intent.putExtra(PASSWORD_TAG,password);
        startActivity(intent);
        finish();
    }

    private Toast makeWarningToast(String message){
        return Toast.makeText(this, message,Toast.LENGTH_SHORT);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_set_password;
    }
}
