package com.example.adonisandroid.aroundwisedemoproject.login;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adonisandroid.aroundwisedemoproject.HomeScreenActivity;
import com.example.adonisandroid.aroundwisedemoproject.base.BaseActivity;
import com.example.adonisandroid.aroundwisedemoproject.login.repository.DataListener;
import com.example.adonisandroid.aroundwisedemoproject.room.UserEntry;

import butterknife.BindView;

import com.example.adonisandroid.aroundwisedemoproject.R;

import java.util.Objects;

public class SignInUpActivity extends BaseActivity {


    @BindView(R.id.enter_your_phone_number_here)
    EditText phoneNumber;

    @BindView(R.id.password)
    EditText password;

    @BindView(R.id.password_layout)
    LinearLayout passwordLayout;

    @BindView(R.id.forgot_password)
    TextView forget_password;

    @BindView(R.id.send_code_via_sms)
    Button signInButton;

    @BindView(R.id.no_thanks)
    TextView skipButton;

    private UserEntry userEntry;
    private String phoneNumberString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (getUserPrefs().isUserLoggedIn()) {
            goToDiscoverScreen();
            finish();
        }

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!isNetworkAvailable())
                    noInternetDialog().show();
                else {
                    if (passwordLayout.getVisibility() != View.VISIBLE) {

                        phoneNumberString = phoneNumber.getText().toString();

                        if (validatePhoneNumber(phoneNumberString)) {
                            getDemoApp().getUserEntryDataSource().getUserEntry(phoneNumberString, new DataListener<UserEntry>() {
                                @Override
                                public void onSuccess(UserEntry result) {
                                    if (result != null) {
                                        makePasswordLayotutVisible();
                                        userEntry = result;
                                    } else goToSetPasswordScreen();
                                }

                                @Override
                                public void onFailure(String message) {

                                }
                            });
                        } else makeWarningToast(R.string.error_number_message).show();
                    } else {
                        String passwordString = password.getText().toString();

                        if (checkPassword(passwordString, userEntry)) {
                            getUserPrefs().setCurrentUser(userEntry);
                            getUserPrefs().setUserLoggedIn(true);
                            goToDiscoverScreen();
                        } else makeWarningToast(R.string.incorrect_password).show();
                    }
                }
            }
        });

        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToDiscoverScreen();
            }
        });

        forget_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private boolean validatePhoneNumber(String phoneNumber){
        return phoneNumber.matches("^[0-9]{10}$");
    }

    private boolean checkPassword(String password, UserEntry user) {
        return user != null && user.getPassword().equals(password);
    }


    private void makePasswordLayotutVisible(){
        passwordLayout.setVisibility(View.VISIBLE);
        phoneNumber.setClickable(false);
        phoneNumber.setFocusable(false);
    }

    private void goToDiscoverScreen(){
        Intent intent = new Intent(this, HomeScreenActivity.class);
        startActivity(intent);
        finish();
    }

    private void goToSetPasswordScreen(){
        Intent intent = new Intent(this, SetPasswordActivity.class);
        intent.putExtra(PHONE_NUMBER_TAG,phoneNumberString);
        startActivity(intent);
    }

    private Toast makeWarningToast(int message){
        return Toast.makeText(this, message,Toast.LENGTH_SHORT);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_sign_in_up;
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = Objects.requireNonNull(connectivityManager).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private Dialog noInternetDialog(){
        AlertDialog alertDialog = new AlertDialog.Builder(SignInUpActivity.this).create();
        alertDialog.setTitle(getString(R.string.no_connection));
        alertDialog.setMessage(getString(R.string.recommandation_internet));
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        return alertDialog;
    }
}
