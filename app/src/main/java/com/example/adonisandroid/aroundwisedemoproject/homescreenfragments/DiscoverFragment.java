package com.example.adonisandroid.aroundwisedemoproject.homescreenfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.adonisandroid.aroundwisedemoproject.HomeScreenActivity;
import com.example.adonisandroid.aroundwisedemoproject.R;
import com.example.adonisandroid.aroundwisedemoproject.adapters.MovieDiscoverAdapter;
import com.example.adonisandroid.aroundwisedemoproject.adapters.MoviesAdapter;
import com.example.adonisandroid.aroundwisedemoproject.api_retrofit.model.Movie;
import com.example.adonisandroid.aroundwisedemoproject.base.BaseFragment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DiscoverFragment extends BaseFragment {

    @BindView(R.id.movie_recycler_view)
    RecyclerView movieRecyclerView;

    private static final String ARG_TAG = "list_of_items";
    private List<Movie> movieList = new ArrayList<>();
    private Context context;


    public DiscoverFragment(){

    }

    public static DiscoverFragment newInstance(Serializable movieList) {
        DiscoverFragment fragment = new DiscoverFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_TAG, movieList);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments()!=null)
            movieList = (List<Movie>) getArguments().getSerializable(ARG_TAG);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v =  inflater.inflate(R.layout.fragment_discover, container, false);
        ButterKnife.bind(this,v);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((HomeScreenActivity) Objects.requireNonNull(getContext())).changeActionBarItems(true,false,getString(R.string.offers),false);

        MoviesAdapter moviesAdapter = new MovieDiscoverAdapter(movieList, context,createListener(movieList));
        GridLayoutManager movieGridLayout = new GridLayoutManager(context,2);
        movieGridLayout.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int i) {
                if(i%3==0)
                    return 2;
                else return 1;
            }
        });
        movieRecyclerView.setLayoutManager(movieGridLayout);
        movieRecyclerView.setAdapter(moviesAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();

        ((HomeScreenActivity)getContext()).setTabLayoutVisible();
    }
}
