package com.example.adonisandroid.aroundwisedemoproject.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

@Database(entities = {UserEntry.class},version = 1,exportSchema = false )
public abstract class UserDatabase extends RoomDatabase {

    private static final String LOG_TAG = UserDatabase.class.getSimpleName();
    private static final Object LOCK = new Object();
    private static final String DATABASE_NAME = "userlist";
    private static UserDatabase databaseInstance;

    public static UserDatabase getInstance(Context context){
        if (databaseInstance == null){
            synchronized (LOCK) {
                databaseInstance = Room.databaseBuilder(context.getApplicationContext(),
                        UserDatabase.class, UserDatabase.DATABASE_NAME)
                        .allowMainThreadQueries()
                        .build();
            }
        }
        return databaseInstance;
    }

    public abstract UserDao userDao();
}
