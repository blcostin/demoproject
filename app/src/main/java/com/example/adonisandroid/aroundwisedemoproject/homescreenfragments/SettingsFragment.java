package com.example.adonisandroid.aroundwisedemoproject.homescreenfragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.adonisandroid.aroundwisedemoproject.HomeScreenActivity;
import com.example.adonisandroid.aroundwisedemoproject.R;
import com.example.adonisandroid.aroundwisedemoproject.base.BaseFragment;
import com.example.adonisandroid.aroundwisedemoproject.login.SignInUpActivity;
import com.example.adonisandroid.aroundwisedemoproject.login.repository.DataListener;
import com.example.adonisandroid.aroundwisedemoproject.room.UserEntry;
import com.facebook.login.LoginManager;
import java.util.Calendar;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SettingsFragment extends BaseFragment {

    @BindView(R.id.profile_pic_settings)
    ImageView profilePic;

    @BindView(R.id.phone_number_settings)
    TextView phoneNumber;

    @BindView(R.id.first_name_settings)
    EditText firstName;

    @BindView(R.id.last_name_settings)
    EditText lastName;

    @BindView(R.id.birth_date_settings)
    TextView birthDate;

    @BindView(R.id.email_settings)
    EditText email;

    @BindView(R.id.male_button)
    RadioButton maleButton;

    @BindView(R.id.female_button)
    RadioButton femaleButton;

    @BindView(R.id.gender_radio_button)
    RadioGroup radioGroupGender;

    @BindView(R.id.password_settings)
    TextView passwordText;


    @OnClick(R.id.birth_date_settings)
        protected void changeDate(){


        final Calendar calendar = Calendar.getInstance();

        int mYear = calendar.get(Calendar.YEAR);
        int mMonth = calendar.get(Calendar.MONTH);
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        birthDate.setText(monthOfYear+"/"+dayOfMonth+"/"+year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }


    @OnClick(R.id.change_settings_button)
        protected void changePhoneNumber() {
            makeToast(getString(R.string.cannot_be_changed)).show();
    }

    @OnClick(R.id.change_settings_button_2)
        protected void changePassword(){
            changePasswordDialog().show();
        }

    @OnClick(R.id.log_out_button)
        protected void logOut(){

            //log out from app
            getUserPrefs().logout();
            LoginManager.getInstance().logOut();

            makeToast(getString(R.string.logged_out)).show();

            //go to sign in activity
            Intent i = new Intent(context, SignInUpActivity.class);
            startActivity(i);
            getActivity().finish();
    }


    @OnClick(R.id.save_settings_button)
        protected void save(){

            takeChanges();
            getUserPrefs().setCurrentUser(userEntry);


            ((HomeScreenActivity) (Objects.requireNonNull(getActivity()))).getDemoApp().getUserEntryDataSource()
                    .updateUser(getUserPrefs().getCurrentUser(), new DataListener<UserEntry>() {
                @Override
                public void onSuccess(UserEntry data) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            makeToast(getString(R.string.user_updated)).show();
                        }
                    });

                }

                @Override
                public void onFailure(String message) {

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            makeToast(getString(R.string.update_failed)).show();
                        }
                    });
                }
            });
    }

    private String password;
    private UserEntry userEntry;
    private Context context;

    public SettingsFragment() {
        // Required empty public constructor
    }


    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        userEntry = getUserPrefs().getCurrentUser();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, v);

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadData();
        ((HomeScreenActivity) Objects.requireNonNull(getContext())).changeActionBarItems(false, false, getString(R.string.settings), true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context= context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


    private AlertDialog changePasswordDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        LayoutInflater li = LayoutInflater.from(context);
        @SuppressLint("InflateParams") View dialogView = li.inflate(R.layout.change_password_dialog, null);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(dialogView);

        final EditText userInput = dialogView.findViewById(R.id.editTextDialogUserInput);
        final EditText userInput2 =dialogView.findViewById(R.id.editTextDialogUserInput2);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setIcon(R.mipmap.ic_launcher)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {


                                String pass1 = userInput.getText().toString();
                                String pass2 = userInput2.getText().toString();


                                if(pass1.length()>=8) {
                                    if (pass1.equals(pass2)) {
                                        password = pass1;
                                        passwordText.setText(password);
                                    }
                                    else {
                                        makeToast(getString(R.string.invalid_password_data)).show();
                                        keepDialogOpen();
                                    }
                                }
                                else {
                                    makeToast(getString(R.string.minimum_characters)).show();
                                    keepDialogOpen();
                                }
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });
        return alertDialogBuilder.create();
    }

    private void loadData(){

        ((HomeScreenActivity) Objects.requireNonNull(getActivity())).loadProfilePicture(profilePic);


        phoneNumber.setText(userEntry.getPhoneNumber());
        firstName.setText(userEntry.getFirstName());
        lastName.setText(userEntry.getLastName());
        birthDate.setText(userEntry.getBirthDate());
        email.setText(userEntry.getEmailAdress());
        password = userEntry.getPassword();

        String gender = userEntry.getGender();

        if(gender !=null && gender.equals("female"))
            femaleButton.setChecked(true);
        else maleButton.setChecked(true);
    }


    private void takeChanges(){

        userEntry.setFirstName(firstName.getText().toString());
        userEntry.setLastName(lastName.getText().toString());
        userEntry.setEmailAdress(email.getText().toString());
        userEntry.setBirthDate(birthDate.getText().toString());
        userEntry.setPassword(password);


        if (radioGroupGender.getCheckedRadioButtonId()==R.id.female_button)
            userEntry.setGender(getResources().getString(R.string.female));
        else userEntry.setGender(getResources().getString(R.string.male));
    }

    private Toast makeToast(String message){
        return Toast.makeText(context,message,Toast.LENGTH_SHORT);
    }

    private void keepDialogOpen(){
        changePasswordDialog().show();
    }


}
