package com.example.adonisandroid.aroundwisedemoproject.login.repository.userentry;

import com.example.adonisandroid.aroundwisedemoproject.login.repository.DataListener;
import com.example.adonisandroid.aroundwisedemoproject.room.UserEntry;

public interface UserEntryDataSource {

    void getUserEntry(final String phoneNumber, final DataListener<UserEntry> dataListener);
    void insertUser(final UserEntry userEntry);
    void updateUser(final UserEntry userEntry,final DataListener<UserEntry> dataListener);
}
