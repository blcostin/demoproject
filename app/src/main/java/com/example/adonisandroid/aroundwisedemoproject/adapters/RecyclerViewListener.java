package com.example.adonisandroid.aroundwisedemoproject.adapters;

import android.view.View;

public interface RecyclerViewListener {
    void onClick(View view, int position);
}
