package com.example.adonisandroid.aroundwisedemoproject.room;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;


@Entity(tableName = "user")
public class UserEntry {

    @NonNull
    @PrimaryKey
    private String phoneNumber;
    private String password;
    private String firstName;
    private String lastName;
    private String emailAdress;
    private String pictureUrl;
    private String birthDate;
    private String gender;

    @Ignore
    public UserEntry(String phoneNumber, String password, String firstName, String lastName, String emailAdress,String pictureUrl,String birthDate, String gender){
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAdress = emailAdress;
        this.pictureUrl = pictureUrl;
        this.birthDate = birthDate;
        this.gender = gender;
    }

    @Ignore
    public UserEntry(String phoneNumber, String password){
        this(phoneNumber,password,null,null,null,null,null,null);
    }

    public UserEntry(){
    }


    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailAdress() {
        return emailAdress;
    }

    public void setEmailAdress(String emailAdress) {
        this.emailAdress = emailAdress;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

}
