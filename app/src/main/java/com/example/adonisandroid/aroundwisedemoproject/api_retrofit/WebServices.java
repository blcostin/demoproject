package com.example.adonisandroid.aroundwisedemoproject.api_retrofit;

import com.example.adonisandroid.aroundwisedemoproject.api_retrofit.response.GetMovieResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WebServices {
    @GET("3/discover/movie")
    Call<GetMovieResponse> getMovies(@Query("api_key") String apiKeyStr);
}

