package com.example.adonisandroid.aroundwisedemoproject;

import android.app.Application;

import com.example.adonisandroid.aroundwisedemoproject.api_retrofit.ApiClient;
import com.example.adonisandroid.aroundwisedemoproject.api_retrofit.WebServices;
import com.example.adonisandroid.aroundwisedemoproject.login.repository.userentry.UserEntryDataSource;
import com.example.adonisandroid.aroundwisedemoproject.login.repository.userentry.UserEntryRepository;
import com.example.adonisandroid.aroundwisedemoproject.room.UserDatabase;

public class DemoApp extends Application {

    private UserDatabase userDatabase;
    private UserPrefs userPrefs;
    private UserEntryDataSource userEntryDataSource;
    private WebServices webServices;

    @Override
    public void onCreate() {
        super.onCreate();
        userDatabase = UserDatabase.getInstance(getApplicationContext());
        webServices = ApiClient.getWebServices(getString(R.string.base_url));
        userPrefs = new UserPrefs(getApplicationContext());
    }

    public UserDatabase getUserDatabase() {
        return userDatabase;
    }

    public UserEntryDataSource getUserEntryDataSource() {
        if (userEntryDataSource == null) {
            userEntryDataSource = new UserEntryRepository(getUserDatabase());
        }
        return userEntryDataSource;
    }

    public WebServices getWebServices() {
        return webServices;
    }

    public UserPrefs getUserPrefs() {
        return userPrefs;
    }
}
