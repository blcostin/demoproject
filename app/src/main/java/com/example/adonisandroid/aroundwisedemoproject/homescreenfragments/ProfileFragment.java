package com.example.adonisandroid.aroundwisedemoproject.homescreenfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.example.adonisandroid.aroundwisedemoproject.HomeScreenActivity;
import com.example.adonisandroid.aroundwisedemoproject.R;
import com.example.adonisandroid.aroundwisedemoproject.adapters.MovieProfileAdapter;
import com.example.adonisandroid.aroundwisedemoproject.adapters.MoviesAdapter;
import com.example.adonisandroid.aroundwisedemoproject.api_retrofit.model.Movie;
import com.example.adonisandroid.aroundwisedemoproject.base.BaseFragment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ProfileFragment extends BaseFragment {

    @BindView(R.id.tabs_profile)
    TabLayout tabLayout;

    @BindView(R.id.profile_recycler_view)
    RecyclerView profileRecyclerView;

    @BindView(R.id.profile_image_view)
    ImageView pictureProfile;

    private static final String ARG_TAG = "list_of_items_profile";
    private List<Movie> movieList = new ArrayList<>();
    private Context context;

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance(Serializable movieList) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_TAG, movieList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments()!=null)
            movieList = (List<Movie>) getArguments().getSerializable(ARG_TAG);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
//        menu.findItem(R.id.settings_mag_icon).setVisible(true);
//        menu.findItem(R.id.search_mag_icon).setVisible(false);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v =  inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this,v);
        setTabLayoutTitles();

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //set facebook profile picture
        ((HomeScreenActivity) Objects.requireNonNull(getActivity())).loadProfilePicture(pictureProfile);


        //set adapter for movies recycler view
        MoviesAdapter moviesAdapter = new MovieProfileAdapter(movieList, context, createListener(movieList));
        GridLayoutManager movieGridLayout = new GridLayoutManager(context,2);

        profileRecyclerView.setLayoutManager(movieGridLayout);
        profileRecyclerView.setAdapter(moviesAdapter);

        ((HomeScreenActivity) Objects.requireNonNull(getContext())).changeActionBarItems(false,true,getString(R.string.my_profile),false);


//        Objects.requireNonNull(((AppCompatActivity) Objects.requireNonNull(getActivity())).getSupportActionBar()).setTitle(R.string.my_profile);
    }

    private void setTabLayoutTitles() {
        tabLayout.addTab(tabLayout.newTab().setText(R.string.favorites));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.activity));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.my_rewards));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.cards));
    }
}
