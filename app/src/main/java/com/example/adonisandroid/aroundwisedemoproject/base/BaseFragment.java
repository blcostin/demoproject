package com.example.adonisandroid.aroundwisedemoproject.base;

import   android.support.v4.app.Fragment;
import android.view.View;

import com.example.adonisandroid.aroundwisedemoproject.DemoApp;
import com.example.adonisandroid.aroundwisedemoproject.HomeScreenActivity;
import com.example.adonisandroid.aroundwisedemoproject.UserPrefs;
import com.example.adonisandroid.aroundwisedemoproject.adapters.RecyclerViewListener;
import com.example.adonisandroid.aroundwisedemoproject.api_retrofit.model.Movie;
import com.example.adonisandroid.aroundwisedemoproject.homescreenfragments.OfferDetailsFragment;

import java.util.List;
import java.util.Objects;

public class BaseFragment extends Fragment {

    protected UserPrefs getUserPrefs() {
        return getDemoApp().getUserPrefs();
    }

    protected DemoApp getDemoApp() {
        return ((DemoApp) Objects.requireNonNull(getActivity()).getApplication());
    }

    protected RecyclerViewListener createListener(final List<Movie> list) {
        return new RecyclerViewListener() {
            @Override
            public void onClick(View view, int position) {
                ((HomeScreenActivity) Objects.requireNonNull(getActivity())).setTabLayoutInvisible();
                ((HomeScreenActivity) Objects.requireNonNull(getActivity())).changeFragment(OfferDetailsFragment.newInstance(list.get(position)));
            }
        };
    }
}
