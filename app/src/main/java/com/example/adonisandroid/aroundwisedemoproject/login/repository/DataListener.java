package com.example.adonisandroid.aroundwisedemoproject.login.repository;

public interface DataListener<T> {
    void onSuccess(T data);
    void onFailure(String message);
}
